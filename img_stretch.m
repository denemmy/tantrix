function img_res = img_stretch(img, interval)

    min_img = min(img(:));
    max_img = max(img(:));
    delta_img = max_img - min_img;
    
    delta_dst = interval(2) - interval(1);
    
    img_res = interval(1) + delta_dst .* (img - min_img) ./ delta_img;
end