function main(img_path, db_dir)

    %% read image
    img = imread(img_path);
    img = im2double(img);
    
    figure, imshow(img);
    
    %% preprocessing
    img = imgaussfilt(img, 0.3);
    
    if size(img, 3) == 3
        img_gray = rgb2gray(img);
    else
        img_gray = img;
    end
    img_gray = imcomplement(img_gray);
    figure, imshow(img_gray, []);
    
    %% mark foreground    
    bw = smart_binarization(img_gray, 1.2);
    figure, imshow(bw), title('bw');
    %% load db
    load(fullfile(db_dir, 'db.mat'), 'data');
    db_data = data;
    %% segmentation
    stats = regionprops(bw, 'Area', 'BoundingBox', 'Image', 'Perimeter', 'ConvexHull', 'ConvexImage', 'PixelIdxList');
    
    samples_errors = [];
    bboxes = [];    
    
    for i=1:length(stats)
        
        fprintf('processing %d of %d...', i, length(stats));
        
        region_stat = stats(i);
        is_valid = check_region(region_stat);
        
        if ~is_valid
            fprintf('wrong size.\n');
            continue;
        end
        
        bbox = region_stat.BoundingBox;
        rgb_region = imcrop(img, bbox);
        
        region_mask = false(size(bw));
        region_mask(region_stat.PixelIdxList) = true;        
        region_mask = imcrop(region_mask, bbox);
        
        corners_pts = find_corners(region_mask);
        
        if isempty(corners_pts)
            fprintf('failed to find corners.\n');
            continue;
        end
        
        region_nrm = normalize_region(rgb_region, region_mask, corners_pts);
        
        err_values = find_best_match(region_nrm, db_data);
        samples_errors = [samples_errors err_values];
        bboxes = [bboxes; bbox];
        
        fprintf('done.\n');       
    end
    
    n_samples = size(samples_errors, 2);    
    figure, imshow(img);
    hold on;
    title('regions');
    for i=1:n_samples
        [~, min_idx] = min(samples_errors(:));
        [db_id, sample_id] = ind2sub(size(samples_errors), min_idx);
        samples_errors(db_id, :) = samples_errors(db_id, :) .* Inf;
        samples_errors(:, sample_id) = samples_errors(:, sample_id) .* Inf;
        
        x = bboxes(sample_id, 1);
        y = bboxes(sample_id, 2);
        text(x, y, sprintf('%d', db_id));
    end
    
    return;
end

function err_vals = find_best_match(region_nrm, db_data)
    
    feature = compute_feat(region_nrm);

    n_db_images = size(db_data.images_norm, 1);
    n_db_opt = size(db_data.images_norm, 2);
    err_vals = Inf * ones(n_db_images, n_db_opt);
    
    for i=1:size(db_data.images_norm, 1)
        for j=1:size(db_data.images_norm, 2)
            feature_db = db_data.features{i, j};                        
            err_hog = norm(feature - feature_db);
            err_vals(i, j) = err_hog;
        end
    end
    
    err_vals = min(err_vals, [], 2);
end

function is_valid = check_region(region_stat)

    minArea = 300;
    is_valid = true;
    if region_stat.Area < minArea
    	is_valid = false;
        return;
    end
end
