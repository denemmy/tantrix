function bw = smart_binarization(img_gray, level)

    H = fspecial('average', 50);
    img_avg = imfilter(img_gray, H,'replicate');
    
    img_mean = mean(img_gray(:));
    img_min = min(img_gray(:));
    thr = img_min + level * (img_mean - img_min);
    C = 0.05;
    
    bw = img_gray - img_avg - C > 0 & img_gray > thr;
    
    bw = imfill(bw, 'holes');
    
    se = strel('disk', 3);
    bw = imopen(bw, se);

end