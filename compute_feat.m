function feature = compute_feat( region_n )
	region_n = imgaussfilt(region_n, 0.3);
    
    region_g = rgb2gray(region_n);
    
    red_response = region_n(:, :, 1) - region_g;
    red_response = img_stretch(red_response, [0, 1]);
    
    yellow_response = (region_n(:, :, 1) + region_n(:, :, 2)) - 2 * region_g;
    yellow_response = img_stretch(yellow_response, [0, 1]);
    
    blue_response = region_n(:, :, 3) - region_g;
    blue_response = img_stretch(blue_response, [0, 1]);
    
    feature_r = extractHOGFeatures(red_response);
    feature_y = extractHOGFeatures(yellow_response);
    feature_b = extractHOGFeatures(blue_response);
    
    feature = [feature_r feature_y feature_b];
    
end

