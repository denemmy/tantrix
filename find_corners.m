function pts = find_corners(region_mask)

    dist_trn = bwdist(~region_mask);
    
    % find center
    [~, ind] = max(dist_trn(:));
    [row, col] = ind2sub(size(region_mask), ind);
    center = [col, row];
    
    n_steps = 128;
    angle_step = 2 * pi / n_steps;
    angle_vals = angle_step .* (0:n_steps-1);
        
    edge_pts = zeros(n_steps, 2);
    edge_dist = zeros(n_steps, 1);
    
    for i=1:length(angle_vals)
        angle = angle_vals(i);
        [points, mask_along_line] = bresenham_line(region_mask, center, angle);
        
        nnz_ind = find(~mask_along_line, 1, 'first');
        if isempty(nnz_ind)
            nnz_ind = length(mask_along_line);
        end
        edge_pt = points(nnz_ind, :);
        distance_to_c = sqrt(sum((edge_pt - center).^2));
        edge_pts(i, :) = edge_pt;
        edge_dist(i) = distance_to_c;
    end
    
    hexagon_angle = pi / 6;
    min_peak_dist = 0.8 * hexagon_angle / angle_step;
    
    max_peak = max(edge_dist(:));
    [min_peak, min_idx] = min(edge_dist(:));
    min_peak_prominence = 0.3 * (max_peak - min_peak);
    
    edge_dist = circshift(edge_dist, -min_idx);
    edge_pts = circshift(edge_pts, -min_idx);

    % figure;
    % findpeaks(edge_dist, 'MinPeakDistance', min_peak_dist, 'MinPeakProminence', min_peak_prominence);
    [peaks, locs, ~, p] = findpeaks(edge_dist, 'MinPeakDistance', min_peak_dist, 'MinPeakProminence', min_peak_prominence);
    
    if length(peaks) < 6
        % not hexagon
        pts = [];
        return;
    end
    
    % sort by prominence
    [~, sort_idx] = sort(p, 'descend');
    sort_idx = sort_idx(1:6);
    % remove other peaks
    locs_sel = false(1, length(locs));
    locs_sel(sort_idx) = true;
    locs(~locs_sel) = [];
    pts = edge_pts(locs, :);
    
end

function [points, mask_along_line] = bresenham_line(region_mask, center, angle)
    
    x0 = center(1);
    y0 = center(2);
    
    vertical_top = false;
    vertical_bottom = false;
    horizontal_right = false;
    horizontal_left = false;
    
    if angle < 2 * pi / 8
    	horizontal_right = true;
    elseif angle < 3 * 2 * pi / 8
        vertical_top = true;
    elseif angle < 5 * 2 * pi / 8
        horizontal_left = true;
    elseif angle < 7 * 2 * pi / 8
        vertical_bottom = true;
    else
        horizontal_right = true;
    end  
    
    if horizontal_right
    	x1 = size(region_mask, 2);
        y1 = -tan(angle) * (x1 - x0) + y0;
    elseif vertical_top
    	y1 = 1;
        x1 = tan(angle - pi / 2) * (y1 - y0) + x0;
    elseif horizontal_left
    	x1 = 1;
        y1 = -tan(angle) * (x1 - x0) + y0;
    elseif vertical_bottom
    	y1 = size(region_mask, 1);
        x1 = tan(angle - pi / 2) * (y1 - y0) + x0;
    end
    
    x1 = max(min(x1, size(region_mask, 2)), 1);
    x0 = max(min(x0, size(region_mask, 2)), 1);
    y1 = max(min(y1, size(region_mask, 1)), 1);
    y0 = max(min(y0, size(region_mask, 1)), 1);
    
    delta_x = x1 - x0;
    delta_y = y1 - y0;
    err_val = 0;
    
    max_side = max(size(region_mask));
    points = zeros(max_side, 2);
    mask_along_line = false(1, max_side);
    px_id = 1;
    
    if horizontal_right || horizontal_left
    	delta_err = abs(delta_y / delta_x);
        y = y0;
        x_step = 1;
        if x1 < x0
            x_step = -1;
        end
        for x=x0:x_step:x1
            points(px_id, 1) = x;
            points(px_id, 2) = y;
            mask_along_line(px_id) = region_mask(y, x);
            err_val = err_val + delta_err;
            
            if err_val < 0.5
                px_id = px_id + 1;
            else            
                while err_val >= 0.5
                    points(px_id, 1) = x;
                    points(px_id, 2) = y;
                    mask_along_line(px_id) = region_mask(y, x);
                    y = y + sign(y1 - y0);
                    err_val = err_val - 1.0;
                    px_id = px_id + 1;
                end
            end
        end
    else
        delta_err = abs(delta_x / delta_y);
        x = x0;
        y_step = 1;
        if y1 < y0
            y_step = -1;
        end
        for y=y0:y_step:y1
            points(px_id, 1) = x;
            points(px_id, 2) = y;
            mask_along_line(px_id) = region_mask(y, x);
            err_val = err_val + delta_err;
            if err_val < 0.5
                px_id = px_id + 1;
            else            
                while err_val >= 0.5
                    points(px_id, 1) = x;
                    points(px_id, 2) = y;
                    mask_along_line(px_id) = region_mask(y, x);
                    x = x + sign(x1 - x0);
                    err_val = err_val - 1.0;
                    px_id = px_id + 1;
                end
            end
        end
    end    
    points(px_id:end, :) = [];
    mask_along_line(px_id:end) = [];
end