function region_n = normalize_region(region, region_mask, corners_pts)
   
    r_region = region(:, :, 1);
    g_region = region(:, :, 2);
    b_region = region(:, :, 3);    

    r_region(~region_mask) = 0;
    g_region(~region_mask) = 0;
    b_region(~region_mask) = 0;

    region(:, :, 1) = r_region;
    region(:, :, 2) = g_region;
    region(:, :, 3) = b_region; 

    img_sz = 150;
    radius = img_sz / 2;
    center = [(img_sz+1) / 2, (img_sz+1) / 2];
    
    n_angles = 6;
    angle_step = 2 * pi / n_angles;
    angles = angle_step .* (0:n_angles-1) + pi / 2;
    
    fixed_points_x = center(1) + radius * cos(angles);
    fixed_points_y = center(2) - radius * sin(angles);

    moving_points = corners_pts;
    fixed_points = [fixed_points_x', fixed_points_y'];
    tform = fitgeotrans(moving_points, fixed_points, 'projective');
    
    Rcb = imref2d(size(region));
    Rcb.XWorldLimits = [0 img_sz-1];
    Rcb.YWorldLimits = [0 img_sz-1];
    Rcb.ImageSize = [img_sz img_sz];
    
    region_n = imwarp(region, tform, 'OutputView', Rcb);    
        
    region_n = imgaussfilt(region_n, 1);
end
