function generate_db(img_path, db_dir)
    img = imread(img_path);
    figure, imshow(img);
    img = im2double(img);
    
    %% preprocessing
    img = imgaussfilt(img, 0.3);
    
    n_angles = 6;
    
    data = struct();
    data.images = cell(10, 1);
    data.images_g = cell(10, 1);
    data.images_norm = cell(10, n_angles);
    data.features = cell(10, n_angles);
    
    bboxes = zeros(10, 4);
    bboxes(1, :) = [1 83 104 121];
    bboxes(2, :) = [104 81 105 121];
    bboxes(3, :) = [210 76 105 121];
    bboxes(4, :) = [316 75 105 121];
    bboxes(5, :) = [422 75 106 121];
    bboxes(6, :) = [528 74 107 122];
    bboxes(7, :) = [635 74 107 122];
    bboxes(8, :) = [743 74 107 122];
    bboxes(9, :) = [850 76 107 122];
    bboxes(10, :) = [954 78 107 122];
    
    n_bboxes = nnz(sum(bboxes, 2));
    
    for i=1:n_bboxes
        
        fprintf('processing item %d of %d...', i, n_bboxes);
        
        %% extract 1
        st_x = bboxes(i, 1);
        en_x = bboxes(i, 1) + bboxes(i, 3) - 1;

        st_y = bboxes(i, 2);
        en_y = bboxes(i, 2) + bboxes(i, 4) - 1;

        img_c = img(st_y:en_y, st_x:en_x, :);
        data.images{1} = img_c;
        figure, imshow(img_c);
        title(num2str(i));
        
        img_g = rgb2gray(img_c);
        img_g = imcomplement(img_g);        
        %% binarization
        bw = smart_binarization(img_g, 0.8);       
        % figure, imshow(bw);
        %% segmentation
        stats = regionprops(bw, 'Area', 'BoundingBox', 'PixelIdxList');        
        areas_values = [stats.Area];
        
        [~, max_idx] = max(areas_values);
        region_stat = stats(max_idx);
        
        bbox = region_stat.BoundingBox;
        gray_region = imcrop(img_g, bbox);
        rgb_region = imcrop(img_c, bbox);
        
        region_mask = false(size(bw));
        region_mask(region_stat.PixelIdxList) = true;        
        region_mask = imcrop(region_mask, bbox);
        %% find corners        
        corners_pts = find_corners(region_mask);
        
        if isempty(corners_pts)            
            error('Failed to find corners on image');
        end
        
        region_nrm = normalize_region(rgb_region, region_mask, corners_pts);
        data.images_norm{i, 1} = region_nrm;        
        data.features{i, 1} = compute_feat(region_nrm);
        Rcb = imref2d(size(region_nrm));
        
        img_sz = size(region_nrm, 1);
        
        angle_step = 360 / n_angles;
        angles = angle_step .* (1:n_angles-1);
        for r=1:length(angles)            
            region_nrm_r = imrotate(region_nrm, angles(r));
            
            c_x = (size(region_nrm_r, 2) + 1) / 2;
            c_y = (size(region_nrm_r, 1) + 1) / 2;
            
            st_x = round(c_x - img_sz / 2);
            st_y = round(c_y - img_sz / 2);
            
            region_nrm_r = region_nrm_r(st_x:st_x + img_sz - 1, st_y:st_x + img_sz -1, :);
            
            data.images_norm{i, r + 1} = region_nrm_r;
            data.features{i, r + 1} = compute_feat(region_nrm_r);
        end       
        
        data.images{i} = im2double(img_c);
        data.images_g{i} = im2double(img_g);
        
        fprintf('done.\n', i, n_bboxes);
    end
    
    save(fullfile(db_dir, 'db.mat'), 'data');
end

